#include "StandardUserInterface.hpp"

std::ostream * StandardUserInterface::current_out_stream = &std::cout;
std::istream * StandardUserInterface::current_in_stream  = &std::cin;


void StandardUserInterface::setCurrentOutStream(std::ostream& out)
{
	StandardUserInterface::current_out_stream = &out;
}

void StandardUserInterface::setCurrentInStream(std::istream& in)
{
	StandardUserInterface::current_in_stream = &in;
}
