#pragma once

/**
 * File : StandardUserInterface.hpp
 * Provide static class member to use fatser and easier
 * prompt and print on screen (basic user interface)
 */

// STD
#include <iostream>
#include <fstream>

/**
 * Provide a simple interface to prompt
 * and print variables of a program
 */
class StandardUserInterface
{
	public:

		using cstring = const char*;

		/**
		 * Start interface that asks for user input
		 * for a given variable
		 *
		 * @param vName, The variable name
		 * @param var, Pointer to the variable
		 */
		template < typename VarType >
		static void prompt(cstring vName, VarType* var)
		{
			std::cout << "Enter " << vName << " : ";
			(*current_in_stream) >> *var;
			std::cout << std::endl;
		}

		/**
		 * Start interface that asks for user input
		 * for a given variable (variadic)
		 *
		 * @param vName, The variable name
		 * @param var, Pointer to the variable
		 */
		template < typename VarType, typename ...Others >
		static void prompt(cstring vName, VarType* var, Others... others)
		{
			prompt(vName, var);
			prompt(others...);
		}

		/**
		 * Display in standard output the variable 
		 * given in parameter and its name
		 *
		 * @param vName, The variable name
		 * @param var, Value of the variable
		 */
		template < typename VarType >
		static void print(cstring vName, const VarType var)
		{
			(*current_out_stream) << "[" << vName << "] = " << var << std::endl;
		}

		/**
		 * Display in standard output the variable
		 * given in parameter and its name
		 *
		 * @param vName, The variable name
		 * @param var, Value of the variable
		 */
		template < typename VarType, typename ... Others >
		static void print(cstring vName, const VarType var, Others ... others)
		{
			print(vName, var);
			print(others...);
		}

		/**
		 * Sets current out stream
		 */
		static void setCurrentOutStream(std::ostream& out);

		/**
		 * Sets current in stream
		 */
		static void setCurrentInStream(std::istream& in);

	private:

		// Current out streamcurrent_out_stream
		static std::ostream * current_out_stream;

		// Current in stream
		static std::istream * current_in_stream;
};

typedef StandardUserInterface SUI;