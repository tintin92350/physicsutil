#ifndef PHYSIC_CONSTANTS_HEADER
#define PHYSIC_CONSTANTS_HEADER

//------------------------------------------------------------------------------
// Contants expression used in this program
//------------------------------------------------------------------------------
constexpr double PI = 3.1415926535897932; // PI (maths)
constexpr double g  = 9.81;               // Gravity force on earth

#endif
