#ifndef PHYSIC_RENDERER_HEADER
#define PHYSIC_RENDERER_HEADER

#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

//==============================================================================
// Class that open GNUPLOT program as fork and allow program to
// ask for rendering plot on the program
//==============================================================================
class GNUPLOT {

 public:

  GNUPLOT();
  ~GNUPLOT();

  void operator () (const char * command);

  void plot(const char * filename, const int field1, const int field2, const char * title = "");
  void replot(const char * filename, const int field1, const int field2, const char * title = "");
  void plot(const char * func, const char * title = "");
  void replot(const char * func, const char * title = "");
  
 private:

  FILE * program;
};

#endif
