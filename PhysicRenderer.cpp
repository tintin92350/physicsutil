#include "PhysicRenderer.h"

GNUPLOT::GNUPLOT()
{
  program = popen("gnuplot -persist", "w");

  if(!program)
    std::cerr << "GNUPLOT program not found" << std::endl;
}

GNUPLOT::~GNUPLOT()
{
  if(program) {
    fprintf(program, "exit\n");
    pclose(program);
  }
}

void GNUPLOT::operator() (const char * command)
{
  fprintf(program, "%s\n", command);
  fflush(program);
}

void GNUPLOT::plot(const char * filename, const int field1, const int field2, const char * title)
{
  std::string cmd = "plot \"" + std::string(filename) + "\" using " + std::to_string(field1) + ":" + std::to_string(field2) + " with lines title \"" + title + "\"";
  (*this)(cmd.c_str());
}

void GNUPLOT::replot(const char * filename, const int field1, const int field2, const char * title)
{
  std::string cmd = "replot \"" + std::string(filename) + "\" using " + std::to_string(field1) + ":" + std::to_string(field2) + " with lines title \"" + title + "\"";
  (*this)(cmd.c_str());
}

void GNUPLOT::plot(const char * func, const char * title)
{
  std::string cmd = "plot " + std::string(func) + " with lines title \"" + std::string(title) + "\"";
  (*this)(cmd.c_str());
}

void GNUPLOT::replot(const char * func, const char * title)
{
  std::string cmd = "replot " + std::string(func) + " with lines title \"" + std::string(title) + "\"";
  (*this)(cmd.c_str());
}
