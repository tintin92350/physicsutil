#ifndef PHYSIC_UTILS_HEADER
#define PHYSIC_UTILS_HEADER

//------------------------------------------------------------------------------
// Garbage functions
//------------------------------------------------------------------------------
void skipline()
{
  std::string buffer;
  std::getline(std::cin, buffer);
}

//------------------------------------------------------------------------------
// Function that ask for parameter from user and return it
//------------------------------------------------------------------------------
template < typename VariableType >
VariableType inparam(const char * variable)
{
  VariableType out = static_cast<VariableType>(0);
  std::cout << " # Enter the variable " << variable << " = ";
  std::cin >> out;
  skipline();

  return out;
}

//------------------------------------------------------------------------------
// Functions that display parameters
//------------------------------------------------------------------------------
template < typename VariableType >
void outparam(const char * name, const VariableType var, const bool endbuffer = true)
{
  std::cout << " ## " << name << " = " << var;
  if(endbuffer) std::cout << std::endl;
}
template < typename VariableType, typename ... Variables >
void outparam(const char * name, const VariableType var, Variables ... vars)
{
  outparam(name, var, false);
  outparam(vars...);
  std::cout << std::endl;
}

#endif
